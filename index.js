#!/usr/bin/env node


//Modules

const Gender = require('./gender.js')
const promptly = require('promptly');

// Main App

function app(){
	(async () => {
	    let firstName = await promptly.prompt('First name: ');
	    let isLastName = await promptly.prompt('Do you want to include a last name? Y or N ', ['Y', 'N']);
	    if (isLastName.toLowerCase() == 'y'){
	      let lastName = await promptly.prompt('Last name: ');
	      var name = `${firstName} ${lastName}`
	    }
	    else {
	      var name = firstName
	    }; 
	    var results = Gender.checkGender(name);
	    Gender.displayGender(results,name)
	})(); 
};

app();

/* This is old code if using commander package

const program = require('commander')

program
  .version('0.0.1')
  .description('Tell gender of a name');

program
  .command('gender firstame lastname')
  .alias('g')
  .description('Check gender')
  .action((firstname, lastname) => {
  if (lastname.length >0){
  		var name = `${firstname} ${lastname}`
  	}
  	else {
  		var name = firstname
  	}; 
    var results = Gender.checkGender(name);
    Gender.displayGender(results,name)
  });

  program.parse(process.argv)

*/