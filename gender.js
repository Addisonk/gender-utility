//Modules
const gender = require ('gender')
const chalk = require('chalk');

//Gender Funtions to be used by Main App

//Function makes names Title Case
function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   return splitStr.join(' '); 
}

//Funtion to check the gender using gender package
function checkGender(name){
  name = titleCase(name);
  return gender.guess(name)
}

//Funtion to display results with colors based on the gender 
function displayGender(results,name){
  var genColor = 'black'
  if (results.gender == 'male') {
      genColor = chalk.blue
    } else if (results.gender == 'female') {
      genColor = chalk.magenta
    } else {
      return console.log('The gender is unknown')
  }
    console.log(`we are ${Math.floor(results.confidence * 100)}% confident ${name} is ${genColor(results.gender)}`)
}

//Export functions to be used in the Main App
module.exports = {
  checkGender,
  displayGender
}